module github.com/OLTeam-go/sea-store-backend-items

go 1.15

require (
	github.com/alecthomas/template v0.0.0-20190718012654-fb15b899a751
	github.com/go-pg/migrations v6.7.3+incompatible
	github.com/go-pg/pg v8.0.7+incompatible
	github.com/go-playground/universal-translator v0.17.0 // indirect
	github.com/google/uuid v1.1.1
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/joho/godotenv v1.3.0
	github.com/labstack/echo/v4 v4.0.0
	github.com/labstack/gommon v0.3.0 // indirect
	github.com/leodido/go-urn v1.2.0 // indirect
	github.com/mattn/go-colorable v0.1.7 // indirect
	github.com/onsi/ginkgo v1.14.0 // indirect
	github.com/stretchr/testify v1.6.1 // indirect
	github.com/swaggo/echo-swagger v1.0.0
	github.com/swaggo/swag v1.6.3
	github.com/valyala/fasttemplate v1.2.1 // indirect
	golang.org/x/crypto v0.0.0-20200820211705-5c72a883971a // indirect
	golang.org/x/net v0.0.0-20200822124328-c89045814202 // indirect
	golang.org/x/sys v0.0.0-20200826173525-f9321e4c35a6 // indirect
	golang.org/x/text v0.3.3 // indirect
	google.golang.org/protobuf v1.25.0 // indirect
	gopkg.in/check.v1 v1.0.0-20190902080502-41f04d3bba15 // indirect
	gopkg.in/go-playground/validator.v9 v9.31.0
	mellium.im/sasl v0.2.1 // indirect
)
